# -*- coding: utf-8 -*-
#!/usr/bin/python
import binascii
from ctypes import *
from Utils import *

TI_aes_128 = cdll.LoadLibrary("libTI_aes_128.so")

class Node:
    conn = ""
    ip = ""
    port = ""

    mac = ""
    addr = ""
    aes = ""

    lastBeatTime = ""
    _type = ""
    nodeState = ""
    updateTimes = 0
    connState = False
    # param:
    #   data: 来自socket的原始加密数据
    # return:
    #   msgJson: 解析过的 Map格式结果
    def decodeMsg(self, data):
        length = len(data)
        if length <= 0:
            return {}
        dataStr = create_string_buffer(length)

        for i in range(length):
            dataStr[i] = data[i]

        str = "<<< cipher text[%d]:" %(length)
        for ch in dataStr:
            str += hex(ord(ch)) + ", "
        logger.debug( str)

        dataStrP = pointer(dataStr)
        cLength = c_int(length)
        cLengthP = pointer(cLength)
        TI_aes_128.Net_to_ARM(dataStrP, cLengthP)
        dataStr = dataStr[:cLength.value]
        str = "<<< plain text[%d]:" %(cLength.value)
        for ch in dataStr:
            str += hex(ord(ch)) + ", "
        logger.debug( str)

        msgJson = {}
        msgJson["MAGIC_BEGIN"] = binascii.hexlify(dataStr[0])
        msgJson["CHECK"] = binascii.hexlify(dataStr[1])
        msgJson["MAC"] = binascii.hexlify(dataStr[2:10])

        msgJson["CMD"] = binascii.hexlify(dataStr[14])
        msgJson["SESSION"] = binascii.hexlify(dataStr[15])
        msgJson["AES"] = binascii.hexlify(dataStr[16])
        msgJson["LENGTH"] = binascii.hexlify(dataStr[17:19])
        msgJson["BODY"] = binascii.hexlify(dataStr[21:-1])
        msgJson["MAGIC_END"] = binascii.hexlify(dataStr[-1])
        return msgJson

    def encodeMsg(self, msgJson = {}):
        msg = binascii.unhexlify(msgJson["MAC"]) + binascii.unhexlify(msgJson["BODY"])
        length = len(msg)
        keyData = create_string_buffer(length)
        keyData.value = msg
        str = ">>> Keydata:"
        for ch in keyData:
            str += hex(ord(ch)) + ", "
        logger.debug( str)

        CMD = c_uint8(int(msgJson["CMD"], base=16))
        AES = c_uint8(int(msgJson["AES"], base=16))
        bufOut = create_string_buffer(1024)
        lenOut = c_int(0)

        TI_aes_128.ARM_to_Net(CMD, AES, c_uint16(length), pointer(keyData), pointer(bufOut), pointer(lenOut))
        str = ">>> cipher text[%d]:" %(lenOut.value)
        for i in range(lenOut.value):
            str += hex(ord(bufOut[i])) + ", "
        logger.debug( str)
        return bufOut[:lenOut.value]

    # param:
    #   msg: 待发送的数据，Map结构传入
    # return：
    #   NULL
    def send(self, msgJson = {}):
        dataStr = self.encodeMsg(msgJson)
        self.conn.send(dataStr)
        return

    def recv(self, len = 1024):
        data = self.conn.recv(len)
        msg = self.decodeMsg(data)
        ts = time.time()
        self.lastBeatTime = datetime.datetime.now()
        self.updateTimes += 1
        return msg

    def setActive(self, state):
        logger.debug( "here setting state %d", state)
        self.connState = state

    def close(self):
        logger.debug( "here close")
        self.setActive(False)
        self.conn.close()

    def connect(self):
        if not self.getConnState():
            self.conn.connect(self.ip, self.port)
            self.setActive(True)

    def getConnState(self):
        return self.connState

    def getLastBeatTime(self):
        return self.lastBeatTime

    def dump(self):
        return

    def __init__(self, conn, addr):
        self.conn = conn
        self.connState = True
        self.ip = addr[0]
        self.port = addr[1]
        self.lastBeatTime = datetime.datetime.now()

if __name__ == "__main__":
    pass