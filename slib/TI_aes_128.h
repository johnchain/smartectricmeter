#ifndef TI_OPT_AES_H_
#define TI_OPT_AES_H_

void aes_enc_dec(unsigned char *state, unsigned char *key, unsigned char dir);

#include <stdint.h>
#include <stdio.h>
#define uint16 uint16_t 
#define uint8 uint8_t
uint16 transform(uint8 *p,uint16 len);
uint16 restore(uint8 *p,uint16 len);
uint16 Check_code(uint8 *p,uint16 len);
void ARM_to_Net(uint8 CMD, uint8 AES, uint16 len, uint8 *p, uint8* buf_out, int* len_out);
int Net_to_ARM(uint8* recvData, int* recvDataLen);
#endif     /* TI_OPT_AES_H_ */
