#include "stdio.h"
#include "TI_aes_128.h"

int main(){
	int i;
	int len = 16;
	unsigned char Aes_Key_enc[16] = {'J', 'i', 'a', 'n', 'g', 'S', 'u', 'K', 'e', 'J', 'i', 'D', 'a', 'X', 'u', 'e'};
	unsigned char Aes_Key_denc[16] = {'J', 'i', 'a', 'n', 'g', 'S', 'u', 'K', 'e', 'J', 'i', 'D', 'a', 'X', 'u', 'e'};
	unsigned char cmd_buf[64] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e'};
	cmd_buf[0] = 0x7e;
	cmd_buf[15] = 0x7e;
	
	for(i = 2; i < len - 1; i++)
		cmd_buf[1] += cmd_buf[i] & 0xff;
	
	for(i = 0; i < len; i++){
		printf("0x%2x, ", cmd_buf[i]);
	}
	printf("\n=============\n");

	aes_enc_dec(cmd_buf, Aes_Key_enc, 0);
	for(i = 0; i < len; i++){
		printf("0x%2x, ", cmd_buf[i]);
	}
	printf("\n=============\n");
	
	aes_enc_dec(cmd_buf, Aes_Key_denc, 1);
	for(i = 0; i < len; i++){
		printf("0x%2x, ", cmd_buf[i]);
	}
	
	printf("\n\n\n\n");


	uint8_t recvData[] = {0x7e, 0x47, 0xbd, 0x02, 0xac, 0x14, 0x00, 0x4b, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9a, 0x00, 0x05, 0x00, 0x00, 0x7d, 0x02, 0x7d, 0x01, 0x03, 0x03, 0xc2, 0xd2, 0x25, 0x57, 0x9b, 0x8d, 0x88, 0xd1, 0x0f, 0x51, 0xb9, 0xf3, 0xaa, 0x84, 0x7e};
	int recvDataLen = sizeof(recvData);
	printf("origin recvDataLen = %d:\n", recvDataLen);
	for(i = 0; i < recvDataLen; i++){
		printf("0x%2x, ", recvData[i]);
	}
	printf("\n");
	
	Net_to_ARM(recvData, &recvDataLen);
	printf("new recvDataLen = %d:\n", recvDataLen);
	for(i = 0; i < recvDataLen; i++){
		printf("0x%2x, ", recvData[i]);
	}
	printf("\n");

	uint8 sendData[64] = {0};
	uint8 p[64] = {0};
	memcpy(p, recvData + 2, 8);
	memcpy(p + 8, recvData + 21, 16);
	// change mac
	p[0] = 0xff;
	p[7] = 0xff;
	printf("sending... p:\n");
	for(i = 0; i < 24; i++)
		printf("%x, ", p[i]);
	printf("\n");
	int sendDataLen = 0;
	ARM_to_Net(0x9a, 0x05, 24, p, sendData, &sendDataLen);
	printf("sendDataLen = %d\n", sendDataLen);
	for(i = 0; i < sendDataLen; i++){
		printf("0x%2x, ", sendData[i]);
	}
	printf("\n");
	return 0;
}
