#coding:utf-8
'''
Created on 2016年5月7日
@author: baocheng
'''
import platform

DBHOST = "127.0.0.1"
DBPORT = 3306
DBUSER = "root"
DBPWD = "123456"
DBNAME = "SmartElectricMeter"
DBCHAR = "utf8"
DBMAXCACHED = 20
DBMINCACHED = 1
#DBAUTOCOMMIT = True

if platform.system() == 'Darwin':
    DBPWD = "123456"
else:
    DBPWD = "Johnchain111"
