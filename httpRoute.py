import json
from klein import Klein

from Utils import *

app = Klein()

def switchPower(content):
    flag = 0
    try:
        key = content["key"]
        value = content["value"]
        logger.debug("key: %s, value: %s", key, value)
        global connectionListFree
        while not connectionListFree:
            gevent.sleep(0.01)
        connectionListFree = False

        for i in range(len(connections) - 1, -1, -1):
            node = connections[i]
            if node.mac == key and node.getConnState() :
                outJson = {}
                outJson["CMD"] = CMDID_STATUS_RSP
                outJson["AES"] = node.aes
                outJson["MAC"] = node.mac
                if value == "on":
                    outJson["BODY"] = node._type + node.addr + POWERON
                else:
                    outJson["BODY"] = node._type + node.addr + POWEROFF
                node.send(outJson)
                flag = 1
                break
    except KeyError:
        return '{"rst":"fail", "reason":"KeyError"}'
    finally:
        connectionListFree = True

    if flag == 1:
        response = '{"rst":"ok"}'
    else:
        response = '{"rst":"fail", "reason":"node not found"}'
    return response


cmdDispatcher = {
    "switch": switchPower,
}

#curl -XPOST -v -H 'Content-Type: appliction/json' -d '{"function":"switch", "key":"ff02ac14004b12ff", "value":"on"}'  http://localhost:8080/cmd
@app.route('/cmd', methods=['POST'])
def do_post(request):
    print "here in do_post /cmd"
    content = json.loads(request.content.read())

    if content.has_key("function"):
        if cmdDispatcher.has_key(content["function"]):
            return cmdDispatcher[content["function"]](content)

    return ""