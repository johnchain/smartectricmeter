#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logzero
from logzero import logger
import json

import os
import sys
import struct
import threading
import time
import datetime
import platform
from DBOps import db_op

logzero.logfile("/tmp/smartectricmeter-logfile.log", maxBytes=10000, backupCount=10)

CMDID_STATUS = '9a'
CMDID_STATUS_RSP = '9b'

CMDID_LOGIN = '01'
CMDID_LOGIN_RSP = '81'

# 小写字符
STATUSON = 'cc'
STATUSOFF = 'dd'

POWERON = '33'
POWEROFF = '44'
NOACTION = '55'
POWERCLEAR = '66'

ON = 1
OFF = 0

connections = []
connectionListFree = True

CONN_CHECK_FRQ_in_SEC = 10
CONN_TIMEOUT_in_MIN = 10