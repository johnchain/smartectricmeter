# -*- coding: utf-8 -*-
#!/usr/bin/python
from ctypes import *
import logging
import logzero
from logzero import logger
import json

TI_aes_128 = cdll.LoadLibrary("libTI_aes_128.so")
keyStr = create_string_buffer(16)
keyStr.value = "JiangSuKeJiDaXue"
print ", 0x".join(x.encode('hex') for x in keyStr.raw)
keyStrP = pointer(keyStr)
print type(keyStrP)

def crytp():
    directionC = c_byte(0)

    dataStr = create_string_buffer(38)
    dataStr[0] = chr(int(0x7e))
    dataStr[1] = '1'
    dataStr[2] = '2'
    dataStr[3] = '3'
    dataStr[4] = '4'
    dataStr[5] = '5'
    dataStr[6] = '6'
    dataStr[7] = '7'
    dataStr[8] = '8'
    dataStr[9] = '9'
    dataStr[10] = 'a'
    dataStr[11] = 'b'
    dataStr[12] = 'c'
    dataStr[13] = 'd'
    dataStr[14] = 'e'
    dataStr[15] = 'f'
    dataStr[-1] = chr(int(0x7e))

    dataStr1 = dataStr
    dataStrP1 = pointer(dataStr1)

    print ", 0x".join(x.encode('hex') for x in dataStr.raw)

    keyStr1 = create_string_buffer(16)
    keyStr1.value = "JiangSuKeJiDaXue"
    keyStrP1 = pointer(keyStr1)
    TI_aes_128.aes_enc_dec(dataStrP1, keyStrP1, 0)
    print ", 0x".join(x.encode('hex') for x in dataStr1.raw)

    print "=============================="
    keyStr2 = create_string_buffer(16)
    keyStr2.value = "JiangSuKeJiDaXue"
    keyStrP2 = pointer(keyStr2)
    TI_aes_128.aes_enc_dec(dataStrP1, keyStrP2, 1)
    print ", 0x".join(x.encode('hex') for x in dataStr1.raw)

    a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    for i in range(len(a) - 1, -1, -1):
        if a[i] % 2 == 0:
            a.pop(i)
        else:
            print a[i]
    print a

if __name__ == "__main__":
    #crytp()
    logzero.logfile("/tmp/rotating-logfile.log", maxBytes=10000, backupCount=3)
    a = {
        "a": 1,
        "b": 2
    }
    ss = "<<<" + json.dumps(a)
    logger.debug(ss)
    state = False
    logger.debug("hello %d", state)
    logger.info("info")
    logger.warn("warn")
    logger.error("error")

    # This is how you'd log an exception
    try:
        raise Exception("this is a demo exception")
    except Exception as e:
        logger.exception(e)