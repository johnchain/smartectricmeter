#!/usr/bin/env python
# -*- coding: utf-8 -*-
from MySqlConn import Mysql
import time
class DBOperator(object):
    def db_connect(self): 
        self.handler = Mysql()
        return self.handler

    def db_version(self):
        return db_op.handler.getOne("select version()")
    ###################################################################
    def db_insert_meter(self, mac, shotAddr, state = 0, value = 0, insertTime = ""):
        ''' data:
                node_id
                sensor_id
                data
        '''
        # now_time = datetime.now().strftime("%Y%m%d%H%M%S")
        sql_str = "insert into meters(mac, shotAddr, state, value) values(%s, %s, %s, %s)"
        value = [mac, shotAddr, state, value]
        return self.handler.insertOne(sql_str, value)
    ###################################################################
    def db_check_meter(self, mac):
        ''' task:
                transaction_number
        '''
        str_sql = "select count(*) from meters where mac = %s"
        value = [mac]
        vers = self.handler.getOne(str_sql, value)
        return vers and int(vers["count(*)"]) > 0

    ###################################################################
    def db_update_meter(self, mac, shotAddr, state, value, updateTimes):
        '''condition:
                mac
                value
                state
        '''
        str_sql = "update meters set value = %s, state = %s, shotAddr = %s, updateTimes = %s where  mac = %s"
        param = [value, state, shotAddr, updateTimes, mac]
        return self.handler.update(str_sql, param=param)

    ###################################################################
    def db_delete_meter(self, mac):
        '''condition:
                mac
                value
                state
        '''
        str_sql = "delete from meters where  mac = %s"
        param = [mac]
        return self.handler.delete(str_sql, param=param)

    ###################################################################
    def db_close(self):
        if self.handler:
            self.handler.dispose()
db_op = DBOperator()

def main():
    db_op.db_connect()
    try:
        vers = db_op.db_version()
        print "database version is: %s" %vers

        vers = db_op.handler.getOne("select count(*) from accounts")
        print "accounts: %s" %vers['count(*)']

        vers = db_op.db_delete_meter("ee02ac14004b12ee")
        print "delete meter: %s" %vers
        time.sleep(5)
        vers = db_op.db_insert_meter("ee02ac14004b12ee", 32211, 33, 22, 11)
        print "insert meter: %s" %vers

        vers = db_op.db_check_meter("ee02ac14004b12ee")
        print "check meter exists: %s" %vers

        vers = db_op.db_update_meter("ee02ac14004b12ee", 12233, 11, 22, 33)
        print "update meter: %s" %vers

        time.sleep(10)
    finally:
        db_op.db_close()

if __name__ == '__main__':
    main()