# -*- coding: utf-8 -*-

from Node import Node
import binascii
import gevent
from gevent import monkey, spawn
monkey.patch_all()

from socket import *
from Utils import *
from httpRoute import app

exitFlag = False

# return: body hex string
def powerOff():
    pass

def processStatus(node, msgJson):
    #if node.mac == "":
    node.mac = msgJson["MAC"]
    body = binascii.unhexlify(msgJson["BODY"])
    logger.debug( "<<< BODY len = %d, body len = %d", len(msgJson["BODY"]), len(body))
    bodyJson = {}
    bodyJson["TYPE"] = binascii.hexlify(body[0])
    bodyJson["ADD"] = binascii.hexlify(body[1:3])
    bodyJson["POWER"] = binascii.hexlify(body[3:7])
    bodyJson["STATUS"] = binascii.hexlify(body[7])
    logger.debug( "<<< %s", json.dumps(bodyJson))

    if not db_op.db_check_meter(node.mac):
        print "db_insert_meter ", node.mac, " ", bodyJson["ADD"]
        db_op.db_insert_meter(node.mac, int(bodyJson["ADD"], base=16), int(bodyJson["STATUS"], base=16), int(bodyJson["POWER"], base = 16))
    else:
        print "db_update_meter ", node.mac, " ", bodyJson["ADD"]
        db_op.db_update_meter(node.mac, int(bodyJson["ADD"], base=16), int(bodyJson["STATUS"], base=16), int(bodyJson["POWER"], base = 16), node.updateTimes)

    node._type = binascii.hexlify(body[0])
    node.addr = binascii.hexlify(body[1:3])
    node.aes = msgJson["AES"]
    node.mac = msgJson["MAC"]

    outJson = {}
    outJson["CMD"] = CMDID_STATUS_RSP
    outJson["AES"] = node.aes
    outJson["MAC"] = node.mac
    if bodyJson["STATUS"] == STATUSON:
        outJson["BODY"] = node._type + node.addr + NOACTION
    else:
        outJson["BODY"] = node._type + node.addr + NOACTION
    node.send(outJson)

def processLogin(node, msgJson):
    #TODO: do log and check valid
    outJson = {}
    outJson["CMD"] = CMDID_LOGIN_RSP
    outJson["AES"] = msgJson["AES"]
    outJson["MAC"] = msgJson["MAC"]
    outJson["BODY"] = '01'
    node.send(outJson)

cmdDispatcher = {
    CMDID_STATUS: processStatus,
    CMDID_LOGIN: processLogin,
}

def communicate(node):
    global exitFlag
    while True:
        try:
            logger.debug( "waiting message from: %s:%s", node.ip, node.port)
            msgJson = node.recv(1024)
            if not msgJson:
                logger.debug( "data null will break...")
                break
            logger.debug( "<<< %s", json.dumps(msgJson))

            if cmdDispatcher.has_key(msgJson["CMD"]):
                cmdDispatcher[msgJson["CMD"]](node, msgJson)
            else:
                logger.debug( "Critical: unknow cmd!!!")
        except Exception as e:
            logger.exception(e)
            print "catch Exception in communicate"
            break
    logger.debug( "coroutine going down")
    node.setActive(False)

def server(ip, port):
    global exitFlag
    server = socket(AF_INET, SOCK_STREAM)
    server.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    server.bind((ip, port))
    server.listen(5)
    try:
        while not exitFlag:
            logger.debug( "here waiting for connection")
            conn, addr = server.accept()
            logger.debug( "here accepted one connection %s:%d", addr[0], addr[1])
            node = Node(conn, addr)
            connections.append(node)
            spawn(communicate, node)
        logger.debug( "server going down")
    except KeyboardInterrupt:
        print "catch KeyboardInterrupt in server"
        exitFlag = True
    finally:
        server.close()

def connectionManager():
    global exitFlag
    while not exitFlag:
        try:
            global connectionListFree
            if not connectionListFree:
                continue
            connectionListFree = False
            for i in range(len(connections) - 1, -1, -1):
                node = connections[i]
                lastBeatTime = node.getLastBeatTime()
                nowtime = datetime.datetime.now()
                if not node.getConnState():
                    logger.debug( "ConnState error, will close and pop connection %s", node.mac)
                    node.close()
                    # connections.pop(i)
                    connections.remove(node)
                    continue
                elif nowtime - lastBeatTime > datetime.timedelta(minutes = CONN_TIMEOUT_in_MIN):
                    logger.debug( "LastBeatTime timeout, will close and pop connection %s", node.mac)
                    node.close()
                    connections.remove(node)
                    continue
            logger.debug("socketServer: current connections %d", len(connections))
            connectionListFree = True
            gevent.sleep(CONN_CHECK_FRQ_in_SEC)
        except Exception as e:
            print "catch Exception in connectionManager"
            logger.exception(e)
        finally:
            connectionListFree = True

def httpServer(ip, port):
    app.run(ip, port)

def main():
    global exitFlag
    try:
        db_op.db_connect()
        vers = db_op.db_version()
        logger.debug("database version is: %s", vers)
        host = ''
        if platform.system() == 'Darwin':
            host = '127.0.0.1'
        elif platform.system() == 'Linux':
            host = 'www.asuscn.space'
        else:
            logger.debug( "Sorry unsupported system! Only support MacOS and Linux currently")
            exit(0)
        g1 = spawn(server, host, 10091)
        g2 = spawn(connectionManager)
        g3 = spawn(httpServer, host, 8080)

        g1.join()
        g2.join()
        g3.join()
    except KeyboardInterrupt:
        print "catch KeyboardInterrupt in main"
        exitFlag = True
        pass
    except Exception as e:
        print "catch Exception in main"
        logger.exception(e)
        exitFlag = True

if __name__ == '__main__':
    main()
