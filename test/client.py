# -*- coding: utf-8 -*-
#!/usr/bin/python
import platform
from socket import *
from threading import Thread,currentThread
from time import sleep
import sys
sys.path.append('..')
from Node import Node
from Utils import *
import binascii

exitFlag = False

macList = [
        "ffffffffffffffff",
        "eeeeeeeeeeeeeeee",
        "dddddddddddddddd",
        "cccccccccccccccc",
        "bbbbbbbbbbbbbbbb",
        "aaaaaaaaaaaaaaaa",
        "9999999999999999",
        "8888888888888888",
        "7777777777777777",
        "6666666666666666",
        "5555555555555555",
        "4444444444444444",
        "3333333333333333",
        "2222222222222222",
        "1111111111111111",
        "0000000000000000",
        ]
valueList = [
            "0000000f",
            "0000000e",
            "0000000d",
            "0000000c",
            "0000000b",
            "0000000a",
            "00000009",
            "00000008",
            "00000007",
            "00000006",
            "00000005",
            "00000004",
            "00000003",
            "00000002",
            "00000001",
            "00000000"
        ]
addrList = [
            "ffff",
            "eeee",
            "dddd",
            "cccc",
            "bbbb",
            "aaaa",
            "9999",
            "8888",
            "7777",
            "6666",
            "5555",
            "4444",
            "3333",
            "2222",
            "1111",
            "0000"

]
def client(threadID):
    client=socket(AF_INET,SOCK_STREAM)
    addr = ""
    if platform.system() == 'Darwin':
        addr = ('www.asuscn.space',10091)
        # addr = ('127.0.0.1', 10091)
    elif platform.system() == 'Linux':
        addr = ('www.asuscn.space',10091)
    else:
        print "Sorry unsupported system! Only support MacOS and Linux currently"
        exit(0)
    client.connect(addr)
    node = Node(client, addr)
    node._type = "0c"

    outJson = {}
    outJson["CMD"] = CMDID_STATUS
    outJson["AES"] = "05"

    while not exitFlag:
        node.value = valueList[threadID]
        outJson["MAC"] = macList[threadID]
        node.addr = addrList[threadID]
        outJson["BODY"] = node._type + node.addr + node.value + STATUSON

        node.send(outJson)
        dataRecv = client.recv(1024)
        str = "<<< cipher text[%d]:" %(len(dataRecv))
        for ch in dataRecv:
            str += hex(ord(ch)) + ", "
        logger.debug(str)
        if platform.system() == 'Darwin':
            sleep(1)
        else:
            sleep(7 * 60)
    node.close()

if __name__ == '__main__':
    try:
        for i in range(16):
            t = Thread(target = client, args=([i]))
            t.start()

        while True:
            sleep(1)

    except Exception as e:
        print e
        exitFlag = True