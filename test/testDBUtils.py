#coding:utf-8
'''
@author: baocheng
'''
import sys
import time
sys.path.append('..')
from MySqlConn import Mysql

#申请资源
mysql = Mysql()

sqlAll = "select * from meters"
result = mysql.getAll(sqlAll)
if result :
    print "get all"
    for row in result :
        print "%s\t%s"%(row["meterID"],row["mac"])
sqlAll = "select * from accounts"
result = mysql.getMany(sqlAll,2)
if result :
    print "get many"
    for row in result :
        print "%s\t%s"%(row["accountID"],row["lastActiveTime"])

result = mysql.getOne(sqlAll)
print "get one"
print "%s\t%s"%(result["accountID"],result["lastActiveTime"])

str_sql = r'''
                    update meters
                    set value = %s, state = %s, shotAddr = %s, updateTimes = %s
                    where  mac = %s
                    '''
value = [2110, 21111, 21112, 21113, "ee02ac14004b12ee"]
#mysql.begin(autoFlag=1)
updatedRows = mysql.update(str_sql, value)
print "update one"
print "updatedRows = ", updatedRows
time.sleep(15)
#释放资源
mysql.dispose()